const mongoose = require('mongoose');
const router = require('express').Router();
const Jokes = mongoose.model('Jokes');

router.post('/', (req, res, next) => {
  const { body } = req;

  if(!body.title) {
    return res.status(422).json({
      errors: {
        title: 'is required',
      },
    });
  }

  if(!body.joke) {
    return res.status(422).json({
      errors: {
        body: 'is required',
      },
    });
  }

  if (body.categories) {
    body.categories = body.categories.split(',')
  } else {
    body.categories = [];
  }

  const finalJoke = new Jokes(body);
  return finalJoke.save()
      .then(() => res.json({ joke: finalJoke.toJSON() }))
      .catch(next);
});

router.get('/', (req, res, next) => {
  return Jokes.find()
      .sort({ createdAt: 'descending' })
      .then((jokes) => res.json({ jokes: jokes.map(joke => joke.toJSON()) }))
      .catch(next);
});

router.param('id', (req, res, next, id) => {
  return Jokes.findById(id, (err, joke) => {
    if(err) {
      return res.sendStatus(404);
    } else if(joke) {
      req.joke = joke;
      return next();
    }
  }).catch(next);
});

router.get('/:id', (req, res, next) => {
  return res.json({
    joke: req.joke.toJSON(),
  });
});

router.patch('/:id', (req, res, next) => {
  const { body } = req;

  if(typeof body.title !== 'undefined') {
    req.joke.title = body.title;
  }

  if(typeof body.categories !== 'undefined') {
    req.joke.categories = body.categories.split(',');
  }

  if(typeof body.body !== 'undefined') {
    req.joke.body = body.body;
  }

  return req.joke.save()
      .then(() => res.json({ joke: req.joke.toJSON() }))
      .catch(next);
});

router.delete('/:id', (req, res, next) => {
  return Jokes.findByIdAndRemove(req.joke._id)
      .then(() => res.sendStatus(200))
      .catch(next);
});

router.post('/:id/like', (req, res, next) => {
  return Jokes.findOneAndUpdate({_id: req.joke._id}, {$inc : {'likes' : 1}})
      .then(() => res.sendStatus(200))
      .catch(next);
});

router.post('/:id/dislike', (req, res, next) => {
  return Jokes.findOneAndUpdate({_id: req.joke._id}, {$inc : {'dislikes' : 1}})
      .then(() => res.sendStatus(200))
      .catch(next);
});

module.exports = router;