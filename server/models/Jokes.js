const mongoose = require('mongoose');

const { Schema } = mongoose;

const JokesSchema = new Schema({
  title: String,
  joke: String,
  categories : {
    type : Array ,
    'default' : []
  },
  likes: {
    type: Number,
    'default': 0
  },
  dislikes: {
    type: Number,
    'default': 0
  }
}, { timestamps: true });

JokesSchema.methods.toJSON = function() {
  return {
    _id: this._id,
    title: this.title,
    joke: this.joke,
    categories: this.categories,
    likes: this.likes,
    dislikes: this.dislikes,
    createdAt: this.createdAt,
    updatedAt: this.updatedAt,
  };
};

mongoose.model('Jokes', JokesSchema);