import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';

class Form extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      joke: '',
      categories: '',
    };

    this.handleChangeField = this.handleChangeField.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.jokeToEdit) {
      this.setState({
        title: nextProps.jokeToEdit.title,
        joke: nextProps.jokeToEdit.joke,
        categories: nextProps.jokeToEdit.categories,
      });
    }
  }

  handleSubmit(){
    const { onSubmit, jokeToEdit, onEdit } = this.props;
    const { title, joke, categories } = this.state;

    if(!jokeToEdit) {
      return axios.post('http://localhost:8000/api/jokes', {
        title,
        joke,
        categories,
      })
        .then((res) => onSubmit(res.data))
        .then(() => this.setState({ title: '', joke: '', categories: '' }));
    } else {
      return axios.patch(`http://localhost:8000/api/jokes/${jokeToEdit._id}`, {
        title,
        joke,
        categories,
      })
        .then((res) => onEdit(res.data))
        .then(() => this.setState({ title: '', joke: '', categories: '' }));
    }
  }

  handleChangeField(key, event) {
    this.setState({
      [key]: event.target.value,
    });
  }

  render() {
    const { jokeToEdit } = this.props;
    const { title, joke, categories } = this.state;

    return (
      <div className="col-12 col-lg-6 offset-lg-3">
        <input
          onChange={(ev) => this.handleChangeField('title', ev)}
          value={title}
          className="form-control my-3"
          placeholder="Title"
        />
        <textarea
          onChange={(ev) => this.handleChangeField('joke', ev)}
          className="form-control my-3"
          placeholder="Joke"
          value={joke}>
        </textarea>
        <input
          onChange={(ev) => this.handleChangeField('categories', ev)}
          value={categories}
          className="form-control my-3"
          placeholder="Categories"
        />
        <button onClick={this.handleSubmit} className="btn btn-primary float-right">{jokeToEdit ? 'Update' : 'Submit'}</button>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onSubmit: data => dispatch({ type: 'SUBMIT_JOKE', data }),
  onEdit: data => dispatch({ type: 'EDIT_JOKE', data }),
});

const mapStateToProps = state => ({
  jokeToEdit: state.home.jokeToEdit,
});

export default connect(mapStateToProps, mapDispatchToProps)(Form);