import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { ReCaptcha } from 'react-recaptcha-google';

import { Form } from '../Joke';

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.handleDelete = this.handleDelete.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleLike = this.handleLike.bind(this);
    this.handleDislike = this.handleDislike.bind(this);

    this.recaptchaLoaded = this.recaptchaLoaded.bind(this);
    this.verifyCallback = this.verifyCallback.bind(this);

    this.state = {
      isVerified: false,
      token: '',
    }
  }

  recaptchaLoaded() {
    console.log('captcha successfully loaded');
  }

  verifyCallback(response) {
    if (response) {
      this.setState({
        isVerified: true,
        token: response
      });
    }
  }

  componentDidMount() {
    const { onLoad } = this.props;

    axios('http://localhost:8000/api/jokes')
      .then((res) => onLoad(res.data));
  }

  handleDelete(id) {
    const { onDelete } = this.props;

    return axios.delete(`http://localhost:8000/api/jokes/${id}`)
      .then(() => onDelete(id));
  }

  handleEdit(joke) {
    const { setEdit } = this.props;

    setEdit(joke);
  }

  handleLike(id) {
    const { onLike } = this.props;

    return axios.post(`http://localhost:8000/api/jokes/${id}/like`)
        .then(() => onLike(id));
  }

  handleDislike(id) {
    const { onDislike } = this.props;

    return axios.post(`http://localhost:8000/api/jokes/${id}/dislike`)
        .then(() => onDislike(id));
  }

  renderCategoriesBadge(categories) {
    if (!categories) {
      return '';
    }

    return (<p>Categories: {categories.map((category) =>
      <span className="badge badge-info mr-1" key={category}>{category}</span>
      )}</p>);
  }

  render() {
    const { jokes } = this.props;

    if (!this.state.isVerified) {
      return (
        <div className="container">
          <div className="text-center">
            <ReCaptcha
                sitekey="6LcmU4kUAAAAAFkeBN3VmgVnVjuEUzknqFSHTR1a"
                render="explicit"
                theme="dark"
                onloadCallback={this.recaptchaLoaded}
                verifyCallback={this.verifyCallback}
            />
          </div>
        </div>
      );
    } else {
      return (
        <div className="container">
          <div className="row pt-5">
            <div className="col-12 col-lg-6 offset-lg-3">
              <h1 className="text-center">JokeTest</h1>
            </div>
            <Form />
          </div>
          <div className="row pt-5">
            <div className="col-12 col-lg-6 offset-lg-3">
              {jokes.map((joke) => {
                return (
                  <div className="card my-3" key={joke._id}>
                    <div className="card-header font-weight-bold">
                      {joke.title}
                    </div>
                    <div className="card-body">
                      <p>{joke.joke}</p>
                      {this.renderCategoriesBadge(joke.categories)}
                    </div>
                    <div className="card-footer">
                      <div className="row">
                        <button onClick={() => this.handleEdit(joke)} className="btn btn-sm btn-primary mx-3">
                          Edit
                        </button>
                        <button onClick={() => this.handleDelete(joke._id)} className="btn btn-sm btn-danger">
                          Delete
                        </button>
                        <button onClick={() => this.handleLike(joke._id)} className="btn btn-sm btn-primary mx-3">
                          <i className="far fa-thumbs-up"></i> {joke.likes}
                        </button>
                        <button onClick={() => this.handleDislike(joke._id)} className="btn btn-sm btn-danger">
                          <i className="far fa-thumbs-down"></i> {joke.dislikes}
                        </button>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = state => ({
  jokes: state.home.jokes,
});

const mapDispatchToProps = dispatch => ({
  onLoad: data => dispatch({ type: 'HOME_PAGE_LOADED', data }),
  onDelete: id => dispatch({ type: 'DELETE_JOKE', id }),
  setEdit: joke => dispatch({ type: 'SET_EDIT', joke }),
  onLike: id => dispatch({ type: 'LIKE_JOKE', id }),
  onDislike: id => dispatch({ type: 'DISLIKE_JOKE', id }),

});

export default connect(mapStateToProps, mapDispatchToProps)(Home);