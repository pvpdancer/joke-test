export default (state={jokes: []}, action) => {
  switch(action.type) {
    case 'HOME_PAGE_LOADED':
      return {
        ...state,
        jokes: action.data.jokes,
      };
    case 'SUBMIT_JOKE':
      return {
        ...state,
        jokes: ([action.data.joke]).concat(state.jokes),
      };
    case 'DELETE_JOKE':
      return {
        ...state,
        jokes: state.jokes.filter((joke) => joke._id !== action.id),
      };
    case 'SET_EDIT':
      const a = {
        ...state,
        jokeToEdit: action.joke,
      };
      console.log(a);

      return a;
    case 'EDIT_JOKE':
      return {
        ...state,
        jokes: state.jokes.map((joke) => {
          if(joke._id === action.data.joke._id) {
            return {
              ...action.data.joke,
            }
          }
          return joke;
        }),
        jokeToEdit: undefined,
      };
    case 'LIKE_JOKE':
      return {
        ...state,
        jokes: state.jokes.map((joke) => {
          if(joke._id === action.id) {
            joke.likes++;
          }
          return joke;
        }),
      };
    case 'DISLIKE_JOKE':
      return {
        ...state,
        jokes: state.jokes.map((joke) => {
          if(joke._id === action.id) {
            joke.dislikes++;
          }
          return joke;
        }),
      };
    default:
      return state;
  }
};
