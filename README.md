# Joke Test

### Installing packages

Install backend packages

```
cd server/
npm i -S path express body-parser express-session cors errorhandler mongoose morgan
```

Install frontend packages

```
cd client/
npm i
```

### Running the app

```
cd server
node app.js

cd client
npm start
```

### Reference

[Reference](https://github.com/AntonioErdeljac/Blog-Tutorial)